# **Chương Trình Từ Điển Bằng JAVA**
---
## **I. Team H&H :**	
  	      
### 1. Member 

- **Nguyễn Trung Hiếu**

MSSV : 17021247

- **Đặng Hữu Hoàn** 

MSSV : 17021253

---
## **II. Dictionay**

- 1.[**Version 1**](https://bitbucket.org/hieunguyentrung/dictionary-assignment-1/src/master/v1/): Phiên bản từ điển command line sơ khai.

[Link](https://bitbucket.org/hieunguyentrung/dictionary-assignment-1/src/master/v1/) : https://bitbucket.org/hieunguyentrung/dictionary-assignment-1/src/master/v1/

- 2.[**Version 2**](https://bitbucket.org/hieunguyentrung/dictionary-assignment-1/src/master/v2/) : Phiên bản từ điển commandline cải tiến lần 1.

[Link](https://bitbucket.org/hieunguyentrung/dictionary-assignment-1/src/master/v2/) : https://bitbucket.org/hieunguyentrung/dictionary-assignment-1/src/master/v2/

- 3.[**Version 3**](https://bitbucket.org/hieunguyentrung/dictionary-assignment-1/src/master/v3/) : Phiên bản từ điển commandline cải tiến lần 2.

[Link](https://bitbucket.org/hieunguyentrung/dictionary-assignment-1/src/master/v3/) : https://bitbucket.org/hieunguyentrung/dictionary-assignment-1/src/master/v3/

![](https://scontent.fhan3-2.fna.fbcdn.net/v/t1.0-9/43879361_1016596095168798_3656443398685982720_n.jpg?_nc_cat=103&oh=cc35cf418d8339dcfe8668636d92577d&oe=5C400C2C)

- 4.[**GUI_Version**](https://bitbucket.org/hieunguyentrung/dictionary-assignment-1/src/master/DictionaryGUI/) : Phiên bản từ điển nâng cấp đồ họa 

[Link](https://bitbucket.org/hieunguyentrung/dictionary-assignment-1/src/master/DictionaryGUI/) : https://bitbucket.org/hieunguyentrung/dictionary-assignment-1/src/master/DictionaryGUI/

![](https://scontent.fhan3-3.fna.fbcdn.net/v/t1.0-9/44104117_1016596105168797_2918354797876740096_n.jpg?_nc_cat=101&oh=8c542390aac1a510a8a965543b3a7eb3&oe=5C4C17C4)
