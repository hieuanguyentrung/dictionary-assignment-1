import java.util.Scanner;
/**
 * Quan li tu dien
 * ClassName: DictionaryManagement
 * @author Nguyen Trung Hieu
 * @author Dang Huu Hoan
 * @version 1.0
 * 
 */

public class DictionaryCommandline{
    /**
     * Hien thi toan bo tu trong Dictionary
     * @param dict  Dictionary
     */
    public void showAllWords(Dictionary dict){
        int size = dict.arWords.size();
        System.out.printf("\n%-8s    | %-20s\t\t\t| Vietnamese\n\n","No","English");
        for(int i = 0; i < size; i++)
        {
            System.out.printf("%-8d    | %-20s\t\t\t| %s\n",i+1, dict.arWords.get(i).word_target, dict.arWords.get(i).word_explain);
        }      

    }
    /**
     *  commandline so khai 
     * goi ham chen insertFromCommandline()
     * goi ham hien thi showAllWords()
     * @param management
     * @param dict
     */
    public void dictionaryBasic(DictionaryManagement management, Dictionary dict){
        System.out.println("*************H&H Dictionary*************");
        System.out.println("");
        System.out.println("VUI LONG SU DUNG CAC LENH DUOI DAY");
        System.out.println("0: Thoat ung dung");
        System.out.println("1: Them tu vao tu dien");
        System.out.println("2: Hien thi danh sach tu");
        
        Scanner reader = new Scanner(System.in);
        char c = reader.next().charAt(0);

        switch (c) {
            case '0':
                System.out.println("Dang thoat ung dung");
                return;
            case '1':
                dict = management.insertFromCommandline();  
                break;
            case '2':
            this.showAllWords(dict);  
                break;    
            default:
                break;
        }
        System.out.println("");
        dictionaryBasic( management,  dict);
    }

    /**
     * v2: cai tien lan 1
     * goi ham chen insertFromFile()
     * goi ham hien thi showAllWords()
     * goi ham tra tu dictionaryLookup()
     * @param management
     * @param dict
     */
    public void dictionaryAdvanced(DictionaryManagement management, Dictionary dict){
        System.out.println("*************C&C Dictionary V2*************");
        System.out.println("");
        System.out.println("VUI LONG SU DUNG CAC LENH DUOI DAY");
        System.out.println("0: Thoat ung dung");
        System.out.println("1: Them tu vao tu dien bang file");
        System.out.println("2: Hien thi danh sach tu");
        System.out.println("3: Tim kiem tu");
        
        Scanner reader = new Scanner(System.in);
        char c = reader.next().charAt(0);

        switch (c) {
            case '0':
                System.out.println("Dang thoat ung dung");
                return;
            case '1':
                dict = management.insertFromFile();
                break;
            case '2':
            this.showAllWords(dict);  
                break;    
                case '3':
                management.dictionaryLookup(dict);
                break;   
            default:
                break;
        }
        System.out.println("");
        dictionaryAdvanced( management,  dict);
    }
    
    /**
     * v3: cai tien lan 2
     * goi ham them tu insertFromCommandline()
     * goi ham sua tu editFromCommandline()
     * goi ham xoa tu deleteFromCommandline()
     * goi ham hien thi showAllWords()
     * goi ham tra tu dictionarySearcher()
     * @param management
     * @param dict
     */
    public void dictionaryAdvancedVer3(DictionaryManagement management, Dictionary dict){
        System.out.println("*************H&H Dictionary V3*************");
        System.out.println("");
        System.out.println("VUI LONG SU DUNG CAC LENH DUOI DAY");
        System.out.println("0: Thoat ung dung");
        System.out.println("1: Them tu vao tu dien ");
        System.out.println("2: Sua tu ");
        System.out.println("3: Xoa tat ca tu da nhap");
        System.out.println("4: Tra tu ");
        System.out.println("5: Xem toan bo tu ");
        
        Scanner reader = new Scanner(System.in);
        char c = reader.next().charAt(0);
        switch (c) {
        case '0' : 
        	System.out.println("Dang thoat ung dung");
        	return;
        case '1' :
        	dict = management.insertFromCommandline();
        	break;
        case '2' :
        	System.out.println("Nhap tu muon sua : ");
        	dict = management.editFromCommandline();
        	break;
        case '3' : 
        	dict = management.deleteFromCommandline();
        	break;
        case '4' :
        	System.out.println("Nhap tu can tim : ");
        	dict = management.dictionaySearcher();
        	break;
        case '5' :
        	dict = management.showAllWords();
        	break;
        default:    break;
        }
        System.out.println("");
        dictionaryAdvancedVer3( management,  dict);
        
    }
}
