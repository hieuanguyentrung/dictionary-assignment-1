/**
 * Dinh nghia Word gom co tu goc va tu giai nghia
 * ClassName: Word
 * @author Nguyen Trung Hieu
 * @author Dang Huu Hoan 
 * @version 1.0
 * 
 */
public class Word{
    public String word_target, word_explain;
    /**
     * Constructor
     */
    public Word(){}
    /**
     * Constructor
     * @param word_target String
     * @param word_explain String
     */
    public Word(String word_target, String word_explain){
        this.word_target = word_target;
        this.word_explain = word_explain;
    } 
 // getter and setter
    public String getWord_target() {
        return word_target;
    }

    public void setWord_target(String word_target) {
        this.word_target = word_target;
    }

    public String getWord_explain() {
        return word_explain;
    }

    public void setWord_explain(String word_explain) {
        this.word_explain = word_explain;
    }
    
}