import java.util.ArrayList;
import java.util.Scanner;
import java.util.Collections;
import java.util.Comparator;

import java.io.File;
import java.io.FileNotFoundException; 

/**
 * Quan li tu dien
 * ClassName: DictionaryManagement
 * @author Nguyen Trung Hieu
 * @author Dang Huu Hoan 
 * @version 1.0
 * 
 */
public class DictionaryManagement{
	Dictionary dictionary= new Dictionary();
    DictionaryExportToFile docghi= new DictionaryExportToFile();
    Scanner scan = new Scanner(System.in);
    private int dem= 1;
    
    public DictionaryManagement(){
    	dictionary.arWords= docghi.docFile();
    }
    /**
     * chen du lieu vao tu command line
     * @return dict: Dictionary
     */
    public Dictionary insertFromCommandline(){
        int number;
        Scanner sc = new Scanner(System.in);
        System.out.println("Ban hay nhap so luong tu:");
        number = sc.nextInt();
        sc.nextLine();
        for(int i = 0; i<number; i++){            
            Word word = new Word();
            word = this.createWord();            
            dictionary.addWordToDictionary(word);
            docghi.ghiFile(dictionary.arWords);
        }
        System.out.println("--- Them Tu Thanh Cong! ---");
        return dictionary;    
    }

    /**
     * Tao ra 1  {word} bang du lieu nguoi dung nhap
     * @return word Word
     */
    public Word createWord(){
        Scanner sc = new Scanner(System.in);
        String engString, vieString;
        System.out.println("Nhap tu tieng anh:");
        engString = sc.nextLine();
        System.out.println("Nhap nghia tieng viet:");
        vieString = sc.nextLine();
        Word word = new Word(engString, vieString);

        return word;
    }
    
    /**
     * Chen du lieu cho tu dien tu file su dung Scanner
     * @return dict Dictionary
     */
    public Dictionary insertFromFile(){
        File file = new File("dictionaries.txt");
        Dictionary dict = new Dictionary();        
        try {
            Scanner sc = new Scanner(file);            
            Word word ;    
            while(sc.hasNextLine()){
                String stringWord = sc.nextLine();

                //trong file co dinh dang: phan cach giua tu va giai nghia la dau tab
                Scanner s = new Scanner(stringWord).useDelimiter("s*\ts*");
                String target = s.next();
                String explain = s.next();
                word = new Word(target,explain);            
                dict.addWordToDictionary(word);        
            }            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }        
        finally{
            return dict;
        }        
    }

    /**
     * tim kiem tu trong tu dien
     * @param dict Dictionary
     * @return void
     */
    public void dictionaryLookup(Dictionary dict){
    	String seach= scan.nextLine();
    	System.out.println("----------------------------------");
        System.out.println("No   |English    |Vietnamese");
        System.out.println("----------------------------------");
        // Sap Xep
        Collections.sort(dictionary.arWords, new Comparator<Word>() {
            public int compare(Word word0, Word word1) {
                return (word0.getWord_target().compareTo(word1.getWord_target()));
            }
        });
        for(Word i: dictionary.arWords){
            Word w  = (Word) i;
            // Khop tu khi da UpCase
            if(w.getWord_target().toUpperCase().matches(".*"+(seach.toUpperCase())+".*") ||
                w.getWord_explain().toUpperCase().matches(".*"+(seach.toUpperCase())+".*")){
                System.out.printf("%-6d%-12s%-12s", dem++, w.getWord_target(),w.getWord_explain());
                System.out.print("\n----------------------------------\n");
            }
        }
        dem= 1;
    	
    }
    
    /**
     * sua tu tu dien
     * 
     * @return dict
     */
    public Dictionary  editFromCommandline(){
    	Dictionary dict = new Dictionary();
    	System.out.print("English: ");
        String tusua= scan.nextLine();
        Word wor= new Word();
        System.out.println("Sua Thanh: ");
        System.out.print("English: ");
        wor.word_target= scan.nextLine();
        System.out.print("Vietnamese: ");
        wor.word_explain= scan.nextLine();
        for(Word i: dictionary.arWords){
            Word w  = (Word) i;
            if(tusua.toUpperCase().matches(w.getWord_target().toUpperCase())){
            	 w.word_target= wor.word_target;
                 w.word_explain= wor.word_explain;
                 docghi.ghiFile(dictionary.arWords);
                 System.out.println("-----  Sua Thanh Cong!  ------");
            }
            }
    	return dict;
    	
     }
    
    /**
     * xoa tu da nhap
     * 
     * @return dict
     */
     public Dictionary deleteFromCommandline(){
    	 Dictionary dict = new Dictionary();
    	 dictionary.arWords.removeAll(dictionary.arWords);
         // Ghi Lai
         docghi.ghiFile(dictionary.arWords);
         System.out.println("--- Xoa Thanh Cong! ---");
    	 return dict;
     }
     
     /**
      * Tra tu
      * 
      * @return dict
      */
     public Dictionary dictionaySearcher() {
    	 Dictionary dict = new Dictionary();
    	 dictionaryLookup(dict);
    	 return dict;
     }
     /**
      * Xem toan bo danh sach tu
      * 
      * @return dict
      */
     public Dictionary showAllWords() {
    	 Dictionary dict = new Dictionary();
    	 System.out.println("----------------------------------");
         System.out.println("No   |English    |Vietnamese");
         System.out.println("----------------------------------");
         // Sắp xếp
         Collections.sort(dictionary.arWords, new Comparator<Word>() {
             public int compare(Word word0, Word word1) {
                 return (word0.getWord_target().compareTo(word1.getWord_target()));
             }
         });
         for(Word i: dictionary.arWords){
             Word w  = (Word) i;
             System.out.printf("%-6d%-12s%-12s", dem++, w.getWord_target(),w.getWord_explain());
             System.out.print("\n----------------------------------\n");
         }
         dem= 1;
    	 return dict;
     }
    
    
}