import java.util.ArrayList;
/**
 * Lop Dictionary chua mang cac Word{word_target, word_explain}
 * ClassName: Dictionary
 * @author Nguyen Trung Hieu
 * @author Dang Huu Hoan
 * @version 1.0
 * 
 */
public class Dictionary{
    public ArrayList<Word> arWords= new ArrayList<Word>();

    /**
     * Contructor
     */
    public Dictionary(){}
    /**
     * Them Word vao Dictionary
     * @param word Word
     * @return void
     */
    public void addWordToDictionary(Word word){
        arWords.add(word);
    }
    
}