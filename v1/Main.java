/**
 * Lop Main chua ham main chay chuong trinh
 * ClassName: Main
 * @author Nguyen Trung Hieu	
 * @author Dang Huu Hoan
 * @version 1.0
 * 
 */
public class Main{
    public static void main(String[] args){
        DictionaryManagement management = new DictionaryManagement();
        Dictionary dict = new Dictionary();
        DictionaryCommandline dc = new DictionaryCommandline();
        dc.dictionaryBasic(management, dict);
    }
}