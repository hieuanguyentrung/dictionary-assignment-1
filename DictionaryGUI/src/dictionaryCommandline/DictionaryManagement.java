package dictionaryCommandline;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Collections;
import java.util.Comparator;

import java.io.File;
import java.io.FileNotFoundException; 

/**
 * Quan li tu dien
 * ClassName: DictionaryManagement
 * @author Nguyen Trung Hieu
 * @author Dang Huu Hoan 
 * @version 1.0
 * 
 */
public class DictionaryManagement extends Dictionary{
    static Scanner scan = new Scanner(System.in);
    /**
     * chen du lieu vao tu command line
     * @return dict: Dictionary
     */
    public static void insertFromCommandline(){
        int number;
        Scanner sc = new Scanner(System.in);
        System.out.println("Ban hay nhap so luong tu:");
        number = sc.nextInt();
        sc.nextLine();
        for(int i = 0; i<number; i++){            
            Word word = new Word();
            word = createWord();
            arrWords.add(word);
        }
        System.out.println("--- Them Tu Thanh Cong! ---");
    }

    /**
     * Tao ra 1  {word} bang du lieu nguoi dung nhap
     * @return word Word
     */
    public static Word createWord(){
        Scanner sc = new Scanner(System.in);
        String engString, vieString;
        System.out.println("Nhap tu tieng anh:");
        engString = sc.nextLine();
        System.out.println("Nhap nghia tieng viet:");
        vieString = sc.nextLine();
        Word word = new Word(engString, vieString);
        return word;
    }


    /**
     * Chen du lieu cho tu dien tu file su dung Scanner
     * @return dict Dictionary
     */
    public static void insertFromFile(){
        File file = new File("dictionaries.txt");
        Dictionary dict = new Dictionary();        
        try {
            Scanner sc = new Scanner(file);            
            Word word ;    
            while(sc.hasNextLine()){
                String stringWord = sc.nextLine();

                //trong file co dinh dang: phan cach giua tu va giai nghia la dau tab
                Scanner s = new Scanner(stringWord).useDelimiter("s*\ts*");
                String target = s.next();
                String explain = s.next();
                word = new Word(target,explain);            
                arrWords.add(word);
            }            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    /**
     * Doc file duoi .dict vao tu dien
     */
    public static void insertFromFileDICT() {
        try {
            String content = readFile("anhviet109K.dict", Charset.defaultCharset());
            String[] words = content.split("@");
            for (String word: words) {
                String result[] = word.split("\r?\n", 2);
                if (result.length >1) {
                    String wordExplain1 = new String();
                    String wordTarget1 = new String();
                    String wordSound1 = new String();
                    if(result[0].contains("/")) {
                        String firstmeaning = result[0].substring(0, result[0].indexOf("/"));
                        String lastSoundMeaning = result[0].substring(result[0].indexOf("/"), result[0].length());
                        wordTarget1 = firstmeaning;
                        wordSound1 = lastSoundMeaning;
                    }
                    else
                    {
                        wordTarget1 = result[0];
                        wordSound1 = "";
                    }
                    wordExplain1 = result[1];
                    arrWords.add( new Word(wordTarget1.trim(),  wordSound1.trim() , wordExplain1.trim()));
                }
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Doc file duoi .dict
     * @param path duong dan toi file
     * @param encoding encoding
     * @return file  encoded
     * @throws IOException
     */
    public static String readFile(String path, Charset encoding) throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    /**
     * Lookup la ham tim tu
     * @param w la tu can tra nghia
     * @return Word tra ve tu  co word_target la w
     */
    public static Word dictionaryLookup(String w)
    {
        for(int i = 0; i <arrWords.size(); i++)
                if (arrWords.get(i).getWord_target().equals(w)) return arrWords.get(i);
        return new Word(w, "","Tu nay khong co trong tu dien");
    }
    public static ArrayList<String> dictionarySearcher(String w)
    {
        ArrayList<String> list = new ArrayList<String>();
        for(Word word :arrWords)
            if (word.getWord_target().startsWith(w)) list.add(word.getWord_target());
        return list;
    }

    /**
     * addWord la ham them 1 tu vao tu dien hien tai
     * @param target la tu tieng anh
     * @param explain la nghia tieng viet
     * @param sound la phien am
     * @return tra ve status
     */
    public static String addWordFromApp(String target,String sound, String explain)
    {
        if (target.equals("")) return "Viet tu cua ban";
        for(Word word : arrWords)
        if(word.getWord_target().equals(target)) return "Tu nay da co trong tu dien";
        if (explain.equals("")) return "Viet nghia cua tu";
        if (sound.equals("")) return "Viet phien am cua tu";
        if (!sound.startsWith("/") || !sound.endsWith("/")) return "phonetic sai format";
        Word word = new Word(target,sound,explain);
        arrWords.add(word);
        return "Them tu thanh cong !";
    }

    /**
     * removeWord la ham xoa 1 tu khoi tu dien
     * @param target la tu tieng anh
     * @return tra ve status
     */
    public static String removeWordFromApp(String target) {
        if (target.equals("")) return "Viet tu can xoa";
        for (Word word : arrWords)
            if (word.getWord_target().equals(target))
            {
                arrWords.remove(word);
                return "Xoa tu thanh cong !";
            }
        return "Tu nay chua co trong tu dien";
    }

    /**
     * editWord la ham sua 1 tu trong tu dien
     * @param target la tu can sua
     * @param explain la nghia moi cua tu do
     * @return tra ve status
     */
    public static String editWordFromApp(String target, String explain)
    {
        if (target.equals("")) return "Viet tu cua ban";
        if (explain.equals("")) return "Viet nghia cua tu";
        for(Word word :arrWords)
            if(word.getWord_target().equals(target)) {
                //arrWords.remove(word);
                word.setWord_explain(explain);
                //arrWords.add(word);
                return "Sua tu thanh cong !";
            }
        return  "Tu nay khong ton tai";
    }

}