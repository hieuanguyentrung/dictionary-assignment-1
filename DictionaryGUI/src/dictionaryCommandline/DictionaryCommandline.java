package dictionaryCommandline;

import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
/**
 * Quan li tu dien
 * ClassName: DictionaryManagement
 * @author Nguyen Trung Hieu
 * @author Dang Huu Hoan
 * @version 1.0
 * 
 */

public class DictionaryCommandline extends DictionaryManagement{
    /**
     * tim kiem tu trong tu dien tu commandline
     * @return void
     */
    public static void dictionaryLookupCml() {
        String seach = scan.nextLine();
        System.out.println("----------------------------------");
        System.out.println("No   |English    |Vietnamese");
        System.out.println("----------------------------------");
        // Sap Xep
        Collections.sort(arrWords, new Comparator<Word>() {
            public int compare(Word word0, Word word1) {
                return (word0.getWord_target().compareTo(word1.getWord_target()));
            }
        });
        for (Word i : arrWords) {
            Word w = (Word) i;
            // Khop tu khi da UpCase
            if (w.getWord_target().toUpperCase().matches(".*" + (seach.toUpperCase()) + ".*") ||
                    w.getWord_explain().toUpperCase().matches(".*" + (seach.toUpperCase()) + ".*")) {
                System.out.printf("%-6d%-12s%-12s", i, w.getWord_target(), w.getWord_explain());
                System.out.print("\n----------------------------------\n");
            }
        }
    }
    /**
     * sua tu tu dien
     */
    public void  editFromCommandline(){
        System.out.print("English: ");
        String tusua= scan.nextLine();
        Word wor = new Word();
        System.out.println("Sua Thanh: ");
        System.out.print("English: ");
        wor.setWord_target(scan.nextLine());
        System.out.print("Vietnamese: ");
        wor.setWord_explain(scan.nextLine());
        for(Word i: arrWords){
            Word w  = (Word) i;
            if(tusua.toUpperCase().matches(w.getWord_target().toUpperCase())){
                w.setWord_target(wor.getWord_target());
                w.setWord_explain(wor.getWord_explain());
                System.out.println("-----  Sua Thanh Cong!  ------");
            }
        }

    }
    /**
     * xoa tu da nhap
     */
    public static void deleteFromCommandline()
    {
        System.out.println("Nhap tu can xoa");
        String target = scan.nextLine();
        arrWords.remove(new Word(target));
        System.out.println("Xoa tu thanh cong");
    }
    /**
     * Tra tu
     * @return dict
     */
    public Dictionary dictionaySearcherCml() {
        Dictionary dict = new Dictionary();
        dictionaryLookupCml();
        return dict;
    }
    /**
     * Hien thi toan bo tu trong Dictionary
     */
    public static void showAllWords(){
        int size = arrWords.size();
        System.out.printf("\n%-8s    | %-20s\t\t\t| Vietnamese\n\n","No","English");
        for(int i = 0; i < size; i++)
        {
            System.out.printf("%-8d    | %-20s\t\t\t| %s\n",i+1, arrWords.get(i).getWord_target(),arrWords.get(i).getWord_explain());
        }      

    }
    /**
     *  commandline so khai
     * goi ham chen insertFromCommandline()
     * goi ham hien thi showAllWords()
     */
    public void dictionaryBasic(){
        System.out.println("*************H&H Dictionary*************");
        System.out.println("");
        System.out.println("VUI LONG SU DUNG CAC LENH DUOI DAY");
        System.out.println("0: Thoat ung dung");
        System.out.println("1: Them tu vao tu dien");
        System.out.println("2: Hien thi danh sach tu");
        
        Scanner reader = new Scanner(System.in);
        char c = reader.next().charAt(0);

        switch (c) {
            case '0':
                System.out.println("Dang thoat ung dung");
                return;
            case '1':
                insertFromCommandline();
                break;
            case '2':
                showAllWords();
                break;    
            default:
                break;
        }
        System.out.println("");
        dictionaryBasic();
    }

    /**
     * v2: cai tien lan 1
     * goi ham chen insertFromFile()
     * goi ham hien thi showAllWords()
     * goi ham tra tu dictionaryLookup()
     */
    public void dictionaryAdvanced(){
        System.out.println("*************C&C Dictionary V2*************");
        System.out.println("");
        System.out.println("VUI LONG SU DUNG CAC LENH DUOI DAY");
        System.out.println("0: Thoat ung dung");
        System.out.println("1: Them tu vao tu dien bang file");
        System.out.println("2: Hien thi danh sach tu");
        System.out.println("3: Tim kiem tu");
        
        Scanner reader = new Scanner(System.in);
        char c = reader.next().charAt(0);

        switch (c) {
            case '0':
                System.out.println("Dang thoat ung dung");
                return;
            case '1':
                insertFromFile();
                break;
            case '2':
            this.showAllWords();
                break;    
                case '3':
                dictionaryLookupCml();
                break;   
            default:
                break;
        }
        System.out.println("");
        dictionaryAdvanced();
    }
    
    /**
     * v3: cai tien lan 2
     * goi ham them tu insertFromCommandline()
     * goi ham sua tu editFromCommandline()
     * goi ham xoa tu deleteFromCommandline()
     * goi ham hien thi showAllWords()
     * goi ham tra tu dictionarySearcher()
     */
    public void dictionaryAdvancedVer3(){
        System.out.println("*************H&H Dictionary V3*************");
        System.out.println("");
        System.out.println("VUI LONG SU DUNG CAC LENH DUOI DAY");
        System.out.println("0: Thoat ung dung");
        System.out.println("1: Them tu vao tu dien ");
        System.out.println("2: Sua tu ");
        System.out.println("3: Xoa tu");
        System.out.println("4: Tra tu ");
        System.out.println("5: Xem toan bo tu ");
        
        Scanner reader = new Scanner(System.in);
        char c = reader.next().charAt(0);
        switch (c) {
        case '0' : 
        	System.out.println("Dang thoat ung dung");
        	return;
        case '1' :
        	insertFromCommandline();
        	break;
        case '2' :
        	System.out.println("Nhap tu muon sua : ");
        	editFromCommandline();
        	break;
        case '3' : 
        	deleteFromCommandline();
        	break;
        case '4' :
        	System.out.println("Nhap tu can tim : ");
        	dictionaySearcherCml();
        	break;
        case '5' :
        	showAllWords();
        	break;
        default:    break;
        }
        System.out.println("");
        dictionaryAdvancedVer3();
        
    }
}
