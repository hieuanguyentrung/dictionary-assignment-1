package dictionaryCommandline;

/**
 * Dinh nghia Word gom co tu goc va tu giai nghia
 * ClassName: Word
 *
 * @author Nguyen Trung Hieu
 * @author Dang Huu Hoan
 * @version 1.0
 */
public class Word {
    private String word_target, word_explain, word_sound;
    private boolean isMarked;

    /**
     * constructor
     */
    public Word(){};
    /**
     * constructor
     * @param target tu tieng anh
     */
    public Word(String target) {
        setWord_target(target);
    }
    /**
     * Constructor
     * @param word_target  tu tieng anh
     * @param word_explain tieng viet
     */
    public Word(String word_target, String word_explain) {
        this.word_target = word_target;
        this.word_explain = word_explain;
    }
    /**
     * Constructor
     *
     * @param word_target  tu tieng anh
     * @param word_explain tieng viet
     * @param word_sound   phat am
     */
    public Word(String word_target, String word_sound, String word_explain) {
        this.word_target = word_target;
        this.word_explain = word_explain;
        this.word_sound = word_sound;
    }

    /**
     * get infor
     *
     * @return String tra ve tu vung , phat am va nghia cua tu
     */
    public String toString() {
        return getWord_target() + "    " + getWord_sound() + "\n" + getWord_explain();
    }

    /**
     * get infor
     *
     * @return String tra ve tu vung, phat am
     */
    public String toString2() {
        return getWord_target() + "    " + getWord_sound();
    }

    // getter and setter
    public String getWord_target() {
        return word_target;
    }

    public void setWord_target(String word_target) {
        this.word_target = word_target;
    }

    public String getWord_explain() {
        return word_explain;
    }

    public void setWord_explain(String word_explain) {
        this.word_explain = word_explain;
    }

    public void setWord_sound(String word_sound) { this.word_sound = word_sound; }

    public String getWord_sound() { return word_sound; }

    public boolean isMarked() { return isMarked; }

    public void setMarked(boolean marked) { isMarked = marked; }

}