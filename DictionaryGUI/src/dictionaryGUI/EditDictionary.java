package dictionaryGUI;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import java.net.URL;
import java.util.ResourceBundle;

public class EditDictionary {
    @FXML
    private TextField TargetAdd, Phonetic, TargetRemove, TargetEdit;
    @FXML
    TextArea ExplainAdd, ExplainRemove, ExplainEdit;
    @FXML
    private Label warningAdd, warningRemove, warningEdit;
    @FXML
    Button addbutton, editbutton, removebutton;
    /**
     * Add words
     */
    public void addWord()
    {
        String target = TargetAdd.getText();
        String sound = Phonetic.getText();
        String explain = ExplainAdd.getText();
        System.out.println(target);
        System.out.println(sound);
        System.out.println(explain);
    }
    /**
     * Remove words
     */
    public void removeWord()
    {
        String target = TargetRemove.getText();
      //  warningRemove.setText(dictionary.removeWordFromApp(target));
        ExplainRemove.setText("");
    }
    /**
     * Edit words
     */
    public void editWord()
    {
        String target = TargetEdit.getText();
        String explain = ExplainEdit.getText();
       // warningEdit.setText(dictionary.editWordFromApp(target,explain));
        ExplainEdit.setText("");
    }
}