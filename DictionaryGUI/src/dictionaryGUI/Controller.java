package dictionaryGUI;

import com.voicerss.tts.*;
import dictionaryCommandline.DictionaryManagement;
import dictionaryCommandline.Word;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    /**
     * class Xcell them button marked vao listview
     */
    static class CellwithButton extends ListCell<String> {
        HBox hbox = new HBox();
        Label label = new Label("(empty)");
        Pane pane = new Pane();
        ToggleButton button = new ToggleButton();
        String lastItem;

        public CellwithButton() {
            super();
            hbox.getChildren().addAll(label, pane, button);
            HBox.setHgrow(pane, Priority.ALWAYS);
            label.setMaxWidth(500);
            label.setMaxHeight(90);
            label.setWrapText(true);
            button.selectedProperty().addListener(((observable, oldValue, newValue) -> {
                String target = lastItem;
                for( int i = 0; i < dictionary.arrWords.size(); i++)
                if (dictionary.arrWords.get(i).getWord_target().equals(target))
                {   dictionary.arrWords.get(i).setMarked(newValue);
                }
                if (newValue) {
                    if (!dictionary.markedWords.contains(target)) dictionary.markedWords.addFirst(target);

                }else
                    dictionary.markedWords.remove(target);
            }));
        }

        @Override
        protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            setText(null);  // No text in label of super class
            if (empty) {
                lastItem = null;
                setGraphic(null);
            } else {
                lastItem = item;
                if (item!=null) {
                    Word word = dictionary.dictionaryLookup(item);
                    button.setSelected(word.isMarked());
                    label.setText(word.toString2());
                }
                setGraphic(hbox);
            }
        }
    }

    public static DictionaryManagement dictionary  = new DictionaryManagement();
    @FXML
    private TextField WordTarget;
    @FXML Label WordShow;
    @FXML private TextArea WordExplain;
    @FXML private ListView<String> listView = new ListView<String>(), listViewMarked = new ListView<String>();
    @FXML private ToggleButton buttonMarked;
    @FXML
    private TextField TargetAdd, Phonetic, TargetRemove, TargetEdit;
    @FXML
    TextArea ExplainAdd, ExplainRemove, ExplainEdit;
    @FXML
    private Label warningAdd, warningRemove, warningEdit;
    /**
     * handle event cho  buttonSound
     * @param event
     * @throws Exception
     */
    public void clickForSound(MouseEvent event) throws Exception{

        String word = WordTarget.getText();
        if (word.trim().equals("")) return;
        System.out.println(word);
        VoiceProvider tts = new VoiceProvider("1d7d26040c284a6ba91ecb53977ee3f0");
        VoiceParameters params = new VoiceParameters(word, Languages.English_UnitedStates);
        params.setCodec(AudioCodec.WAV);
        params.setFormat(AudioFormat.Format_44KHZ.AF_44khz_16bit_stereo);
        params.setBase64(false);
        params.setSSML(false);
        params.setRate(0);
        byte[] voice = tts.speech(params);
        FileOutputStream fos = new FileOutputStream("output_mp3/voice.mp3");
        fos.write(voice, 0, voice.length);
        fos.flush();
        fos.close();
        String gongFile = "output_mp3/voice.mp3";
        InputStream in = new FileInputStream(gongFile);
        AudioStream audioStream = new AudioStream(in);
        AudioPlayer.player.start(audioStream);
    }

    /**
     * Ham lookup cho App
     */
    public void DictionaryLookup()
    {
        String target = WordTarget.getText();
        Word word = dictionary.dictionaryLookup(target);
        WordShow.setText(target + "    " + word.getWord_sound());
        WordExplain.setText(word.getWord_explain());
        buttonMarked.setSelected(word.isMarked());
    }
    /**
     * Ham goi y tu khi go
     */
    public void DictionarySuggestion()
    {
        String target = WordTarget.getText();
        listView.getItems().clear();
        if (target.equals("")) return;
        ArrayList<String> list = dictionary.dictionarySearcher(target);
        ObservableList<String> data = FXCollections.observableArrayList(list);
        listView.getItems().addAll(data);
    }
    /**
     * Ham them cac tu da danh dau vao list view
     */
    public void printMarkedWord()
    {
        listViewMarked.getItems().clear();
        for(int i = 0; i< dictionary.markedWords.size(); i++)
            listViewMarked.getItems().add(dictionary.markedWords.get(i));
    }
    /**
     * ham tao cua so moi
     * @param loc dia chi file fxml
     * @param title Tieu de cua cua so
     * @param parentStage Stage
     */
    public void loadWindow(URL loc, String title, Stage parentStage) {
        try {
            Parent parent = FXMLLoader.load(loc);
            Stage stage = null;
            if(parentStage != null) {
                stage = parentStage;
            } else {
                stage = new Stage(StageStyle.DECORATED);
            }
            stage.setTitle(title);
            stage.setScene(new Scene(parent));
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
    }

    /**
     * handle button Add cho cua so chinh
     */
    public void AddWord()
    {
        loadWindow(getClass().getResource("addWindow.fxml"), "Add Word", null);
    }
    /**
     * handle button add cho cua so phu
     */
    public void addWord()
    {
        String target = TargetAdd.getText();
        String sound = Phonetic.getText();
        String explain = ExplainAdd.getText();
        warningAdd.setText(dictionary.addWordFromApp(target,sound,explain));
    }
    /**
     * handle button Remove cho cua so chinh
     */
    public void RemoveWord()
    {
        loadWindow(getClass().getResource("removeWindow.fxml"), "Remove Word", null);
    }
    /**
     * handle button remove cho cua so phu
     */
    public void removeWord()
    {
        String target = TargetRemove.getText();
        warningRemove.setText(dictionary.removeWordFromApp(target));
    }

    /**
     * Handle button Search khi remove word
     */
    public void searchWhenRemoving()
    {
        String target = TargetRemove.getText();
        Word word = dictionary.dictionaryLookup(target);
        ExplainRemove.setText(word.getWord_explain());
    }
    /**
     * handle button Remove cho cua so chinh
     */
    public void EditWord()
    {
        loadWindow(getClass().getResource("editWindow.fxml"), "Edit Word", null);
    }
    /**
     * handle button edit cho cua so phu
     */
    public void editWord()
    {
        String target = TargetEdit.getText();
        String explain = ExplainEdit.getText();
        warningEdit.setText(dictionary.editWordFromApp(target,explain));
        ExplainEdit.setText("");
    }
    /**
     * Handle button Search khi edit word
     */
    public void searchWhenEditing()
    {
        String target = TargetEdit.getText();
        Word word = dictionary.dictionaryLookup(target);
        ExplainEdit.setText(word.getWord_explain());
    }

    /**
     * Ham khoi tao bien
     * @param location duong dan toi file fxml
     * @param resources resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if(!location.toExternalForm().endsWith("mainWindow.fxml")) return;
        buttonMarked.selectedProperty().addListener(((observable, oldValue, newValue) -> {
            String target = WordTarget.getText();
            if (!target.trim().equals("Hello    /hello/"))
            {
                for( int i = 0; i < dictionary.arrWords.size(); i++)
                    if (dictionary.arrWords.get(i).getWord_target().equals(target))
                    {   dictionary.arrWords.get(i).setMarked(newValue);
                    }
                if (newValue) {
                    if (!dictionary.markedWords.contains(target)) dictionary.markedWords.addFirst(target);
                }
                else
                    dictionary.markedWords.remove(target);
            }
            else buttonMarked.setSelected(false);
        }));

        WordTarget.setOnKeyPressed(new EventHandler<KeyEvent>() {
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    DictionaryLookup();
                }
                if (keyEvent.getCode() == KeyCode.DOWN){
                    listView.requestFocus();
                    listView.getSelectionModel().selectFirst();
                }
            }
        });
        listView.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                String target =  listView.getSelectionModel().getSelectedItem();
                WordTarget.setText(target);
                DictionaryLookup();
            }
        });
        listView.setOnKeyPressed(new EventHandler<KeyEvent>() {
            public void handle(KeyEvent ke) {
                if (ke.getCode() == KeyCode.ENTER) {
                    String target =  listView.getSelectionModel().getSelectedItem();
                    WordTarget.setText(target);
                    DictionaryLookup();
                }
                if (ke.getCode() == KeyCode.UP){
                    if (listView.getSelectionModel().getSelectedIndex() == 0)
                        WordTarget.requestFocus();
                }
            }
        });
        listViewMarked.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> param) {
                return new CellwithButton();
            }
        });
        listView.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                String target =  listView.getSelectionModel().getSelectedItem();
                WordTarget.setText(target);
                DictionaryLookup();
            }
        });
        
        dictionary.insertFromFileDICT();

    }
}
